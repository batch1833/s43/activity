//console.log("Hello World");
// Scenario: Let's create a simple progrm that will perform CRUD operation for posting the available movies to watch.

//Mock Database
let posts = [];
//Post ID
let count = 1;

//Add post data
//This will trigger and event that will add a new post in our mock database upon clicking the "Create" button.

document.querySelector("#form-add-post").addEventListener("submit", (e) =>{

	// We can prevent the page form loading
	e.preventDefault();

	posts.push({
		// "id" property will be used for the unique identification of each posts.
		id: count,
		//title & body values will come  from the "form-add-post" input elements.
		title:document.querySelector("#txt-title").value,
		body: document.querySelector("#txt-body").value
	});

	//count will increment every time a new movie is added.
	count++;

	console.log(posts);
	alert("Successfully Added!");
	showPosts();
})

// View Posts
const showPosts = () =>{
	let postEntries = "";

	// We will used forEach() to display each movie inside our mock database.
	posts.forEach((post) =>{
		postEntries += `
					<div id="post-${post.id}">
						<h3 id="post-title-${post.id}">${post.title}</h3>
						<p id="post-body-${post.id}">${post.body}</p>
						<button onclick="editPost('${post.id}')">Edit</button>
						<button onclick="deletePost('${post.id}')">Delete</button>
					</div>
				`
	});

	console.log(postEntries);
	document.querySelector("#div-post-entries").innerHTML = postEntries;
}

//Edit Post Button
// We will create a function that will be called in the onclick() event and will pass the value in the Update form input box.

const editPost = (id) =>{
	//Contain the value of the title and body in a variable.
	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML;

	// Pass the id, title and body of the movie post to be updated in the edit post/form.
	document.querySelector("#txt-edit-id").value = id;
	document.querySelector("#txt-edit-title").value = title;
	document.querySelector("#txt-edit-body").value = body;

}

//Update post.
//This will trigger an event that will update a post upon clicking the Update button.
document.querySelector("#form-edit-post").addEventListener("submit", (e) =>{
	e.preventDefault();

	let postId = document.querySelector("#txt-edit-id").value
	let title = document.querySelector("#txt-edit-title").value
	let body = document.querySelector("#txt-edit-body").value
	
	//output is string ang number
	console.log(postId);

	// Use a loop that woll check each post stored in our mock database.
	//or you can use find index
	for(i = 0; i < posts.length; i++){
		
		
		//Use a if statement to look for the post to be updated using id property.
		if(posts[i].id == postId){
			//Reassign the value of the title and body property.
			posts[i].title = title;
			posts[i].body = body;

			//Invoke the showPosts() to check the updated posts.
			showPosts();


			alert("Successfully updated!");

			break;
		}
	}
})

//Delete
const deletePost = (id) =>{
		for(i = 0; i < posts.length; i++){
		if(posts[i].id == id){
			posts.splice(i,1);
			document.querySelector(`#post-${id}`).remove();
			alert("Successfully deleted!");

			break;
		}
	}

}